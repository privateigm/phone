module.exports = function (app) {
  
  app.get('/', function(req, res) {
    res.sendFile('clients/index.html', {root: __dirname});
  });

};
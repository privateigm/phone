(function () {
  'use strict';
  
  
  angular
  .module('app', ['ui.router'])
  .config(config);
  
  config.$inject = ['$stateProvider', '$urlRouterProvider'];
  function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise(function ($injector, $location) {
      $injector.get('$state').go('home');
    });
    
    $stateProvider
    .state('home', {
      url: '',
      views: {
        'nav': {
          templateUrl: 'clients/nav/header.html'
        },
        'main': {
          templateUrl: 'clients/home.html'
        },
        'footer': {
          templateUrl: 'clients/nav/footer.html'
        }
      }
    })
    .state('home.products', {
      url: '/products',
      views: {
        'main@': {
          templateUrl: 'clients/products/listing.html'
        },
      }
    })
    .state('home.products.product', {
      url: '/:id',
      views: {
        'main@': {
          templateUrl: 'clients/products/offer.html'
        },
      }
    });
  }
})();
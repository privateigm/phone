var express = require('express'),
  path = require('path'),
  methodOverride = require('method-override'),
  config = require('./config'),
  mongoose = require('mongoose');

var app = express();


mongoose.connect(config.database.url.cloud);


app.set('port', process.env.PORT || 8080);
app.set('views', __dirname + 'clients');
app.use(methodOverride());
app.use('/assets', express.static(path.join(__dirname, 'clients/public')));
app.use('/clients', express.static(path.join(__dirname, 'clients')));


var db = require('./model/index');
db.isAdminExist(function(result) {
  if (!result)
    db.createAdmin();
});

require('./routes')(app);


app.listen(app.get('port'), process.env.IP, function () {
  console.log('Express server listenning on port ' + app.get('port'));
});
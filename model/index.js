var db = {};

var UserModel = require('./user');


db.getUserModel = function() {
    return UserModel;
}


db.isAdminExist = function(callback) {
    UserModel.findOne({'role':'admin'}, function(err, user) {
        callback(!!user);
    })
}

db.createAdmin = function(callback) {
    UserModel.create({
        name: 'admin',
        password: '123',
        role: 'admin'
    }, function(err, user) {
       if (err) 
        return {
            status: 'error',
            msg: err
        };
        
        console.log('create admin name: ' + user.name);
        return {
            status: 'success',
            user: user
        };
    });
}



module.exports = db;

